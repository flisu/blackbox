/**
  ******************************************************************************
  * File Name          : freertos.c
  * Description        : Code for freertos applications
  ******************************************************************************
  * This notice applies to any and all portions of this file
  * that are not between comment pairs USER CODE BEGIN and
  * USER CODE END. Other portions of this file, whether 
  * inserted by the user or by software development tools
  * are owned by their respective copyright owners.
  *
  * Copyright (c) 2017 STMicroelectronics International N.V. 
  * All rights reserved.
  *
  * Redistribution and use in source and binary forms, with or without 
  * modification, are permitted, provided that the following conditions are met:
  *
  * 1. Redistribution of source code must retain the above copyright notice, 
  *    this list of conditions and the following disclaimer.
  * 2. Redistributions in binary form must reproduce the above copyright notice,
  *    this list of conditions and the following disclaimer in the documentation
  *    and/or other materials provided with the distribution.
  * 3. Neither the name of STMicroelectronics nor the names of other 
  *    contributors to this software may be used to endorse or promote products 
  *    derived from this software without specific written permission.
  * 4. This software, including modifications and/or derivative works of this 
  *    software, must execute solely and exclusively on microcontroller or
  *    microprocessor devices manufactured by or for STMicroelectronics.
  * 5. Redistribution and use of this software other than as permitted under 
  *    this license is void and will automatically terminate your rights under 
  *    this license. 
  *
  * THIS SOFTWARE IS PROVIDED BY STMICROELECTRONICS AND CONTRIBUTORS "AS IS" 
  * AND ANY EXPRESS, IMPLIED OR STATUTORY WARRANTIES, INCLUDING, BUT NOT 
  * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY, FITNESS FOR A 
  * PARTICULAR PURPOSE AND NON-INFRINGEMENT OF THIRD PARTY INTELLECTUAL PROPERTY
  * RIGHTS ARE DISCLAIMED TO THE FULLEST EXTENT PERMITTED BY LAW. IN NO EVENT 
  * SHALL STMICROELECTRONICS OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
  * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
  * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, 
  * OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF 
  * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING 
  * NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
  * EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
  *
  ******************************************************************************
  */

/* Includes ------------------------------------------------------------------*/
#include "FreeRTOS.h"
#include "task.h"
#include "cmsis_os.h"

/* Use ARM MATH for Cortex-M4 */
//#include "math_helper.h"

/* USER CODE BEGIN Includes */     
#include <string.h> // strstr, strtok, strcpy
//#include <stdio.h>
#include <stdlib.h> // atof
#include <math.h>
#include "debug.h"
#include "gpio.h"
#include "usart.h"
#include "Accelerometer.h"
//#include "matrix.h"
#define ARM_MATH_CM4
#include "arm_math.h"
#include "core_cm4.h"
/* USER CODE END Includes */

/* Variables -----------------------------------------------------------------*/
//osThreadId defaultTaskHandle;
osThreadId GPSReaderHandle;
osThreadId NMEAParserHandle;
osThreadId LEDblinkerHandle;
osThreadId UDPSenderHandle;
osThreadId MPUreaderHandle;

/* USER CODE BEGIN Variables */
#define MAX_LONGITUDE    180
#define MAX_LATITUDE     90

uint8_t received;
size_t gps_frame_size;
char gps_frame[200];

typedef struct {                                 // Message object structure
	size_t frame_size;
	char frame[200];
} T_MEAS;

typedef struct {
char time[16];
char latitude[15];
char latitude_s[2];
char longitude[15];
char longitude_s[2];
char fix_quality[2];
char satellites[3];
char dillution[4];
char altitude[6];

double lat;
double lon;

uint8_t fix;
} T_GPS_DATA;

osMailQDef(mail, 16, T_MEAS);                    // Define mail queue
osMailQId  mail;
osMailQDef(gps_mail, 16, T_GPS_DATA);                    // Define mail queue
osMailQId  gps_mail;

// KALMAN
MPU_6050 mpu;
float pitch, roll, yaw;
arm_matrix_instance_f32 x_post_p, x_post_r, x_post_y; //float x_post_pitch[2], x_post_roll[2], x_post_yaw[2];
float x_post_pitch[2], x_post_roll[2], x_post_yaw[2];
float d_t;
arm_matrix_instance_f32 A, B, C;  //float A[4], B[2], C[2];
float std_dev_v, std_dev_w;
arm_matrix_instance_f32 V;//float V[4], W[1];
float W;
arm_matrix_instance_f32 P_pri, P_post_p, P_post_r, P_post_y;//float P_pri[4], P_post_pitch[4], P_post_roll[4], P_post_yaw[4];
float P_pri_f32[1], P_post_pitch[4], P_post_roll[4], P_post_yaw[4];
arm_matrix_instance_f32 x_pri;
float x_pri_f32[2];
arm_matrix_instance_f32 eps, S, K; //float eps[1], S[1], K[2];
arm_matrix_instance_f32 uk,yk;//float u[1], y[1];
float acc_x, acc_y;
float u_in[1], y_in[1];
float S_f32[1];
float K_f32[2];
float eps_f32[1];

float Ax_f32[2], Bu_f32[2];
float AP_f32[4], AT_f32[4], APAT_f32[4];
float Cx_f32[1];
float CP_f32[2], CPCT_f32[1];
float PCT_f32[2], S1_f32[1];
float Keps_f32[2];
float KS_f32[2], KSKT_f32[2];
arm_matrix_instance_f32 Ax, Bu;
arm_matrix_instance_f32 AP, AT, APAT;
arm_matrix_instance_f32 Cx;
arm_matrix_instance_f32 CP, CPCT;
arm_matrix_instance_f32 PCT, S1;
arm_matrix_instance_f32 Keps;
arm_matrix_instance_f32 KS, KSKT;
//KALMAN
/* USER CODE END Variables */

/* Function prototypes -------------------------------------------------------*/
//void StartDefaultTask(void const * argument);
void gps_reader(void const * argument);
void nmea_parser(void const * argument);
void led_blinker(void const * argument);
void udp_sender(void const * argument);
void mpu_reader(void const * argument);

void MX_FREERTOS_Init(void); /* (MISRA C 2004 rule 8.1) */

/* USER CODE BEGIN FunctionPrototypes */
void GSM_Init();
void GSM_UDP_Init();
float _nmea2dec(char *nmea, int type, char *dir);
float kalman_filter(arm_matrix_instance_f32*, arm_matrix_instance_f32*, arm_matrix_instance_f32*, arm_matrix_instance_f32*);
/* USER CODE END FunctionPrototypes */

/* Hook prototypes */

/* Init FreeRTOS */

void MX_FREERTOS_Init(void) {
  /* USER CODE BEGIN Init */
	//mail = osMailCreate(osMailQ(mail), NULL); // create mail queue
	//gps_mail = osMailCreate(osMailQ(gps_mail), NULL); // create mail queue
  /* USER CODE END Init */

  /* USER CODE BEGIN RTOS_MUTEX */
  /* add mutexes, ... */
  /* USER CODE END RTOS_MUTEX */

  /* USER CODE BEGIN RTOS_SEMAPHORES */
  /* add semaphores, ... */
  /* USER CODE END RTOS_SEMAPHORES */

  /* USER CODE BEGIN RTOS_TIMERS */
  /* start timers, add new ones, ... */
  /* USER CODE END RTOS_TIMERS */

  /* Create the thread(s) */
  /* definition and creation of defaultTask */
  //osThreadDef(defaultTask, StartDefaultTask, osPriorityNormal, 0, 128);
  //defaultTaskHandle = osThreadCreate(osThread(defaultTask), NULL);

  /* definition and creation of LEDblinker */
  //osThreadDef(LEDblinker, led_blinker, osPriorityBelowNormal, 1, 128);
  //LEDblinkerHandle = osThreadCreate(osThread(LEDblinker), NULL);

  /* definition and creation of GPSReader */
  //osThreadDef(GPSReader, gps_reader, osPriorityNormal, 1, 512);
  //GPSReaderHandle = osThreadCreate(osThread(GPSReader), NULL);

  /* definition and creation of NMEAParser */
  //osThreadDef(NMEAParser, nmea_parser, osPriorityNormal, 1, 1000);
  //NMEAParserHandle = osThreadCreate(osThread(NMEAParser), NULL);

  /* definition and creation of UDPSender */
  //osThreadDef(UDPSender, udp_sender, osPriorityBelowNormal, 1, 1024);
  //UDPSenderHandle = osThreadCreate(osThread(UDPSender), NULL);

  /* definition and creation of MPUreader */
  osThreadDef(MPUreader, mpu_reader, osPriorityBelowNormal, 1, 512);
  MPUreaderHandle = osThreadCreate(osThread(MPUreader), NULL);

  /* USER CODE BEGIN RTOS_THREADS */
  /* add threads, ... */
  /* USER CODE END RTOS_THREADS */

  /* USER CODE BEGIN RTOS_QUEUES */
  /* add queues, ... */
  /* USER CODE END RTOS_QUEUES */
}

/* gps_reader function */
void gps_reader(void const * argument)
{
  /* USER CODE BEGIN gps_reader */
	T_MEAS *mptr = 0;

	memset(gps_frame, '\0', sizeof(gps_frame));
	osDelay(15000);

	HAL_UART_Receive_DMA(&huart6, &received, 1);

	while(1)
	{
		osSignalWait (0x01, osWaitForever); // wait forever for the signal 0x0001

		if((received == '\r') || (received == '\n'))
		{
			if(received == '\r')
			{
				mptr = osMailAlloc(mail, osWaitForever); // Allocate memory
				mptr->frame_size = sprintf(mptr->frame, gps_frame);
				osMailPut(mail, mptr); // Send Mail
				osSignalSet (LEDblinkerHandle, 0x01);
				gps_frame_size = 0;
			}
		}
		else
		{
			gps_frame[gps_frame_size] = received;
			gps_frame_size++;
		}
	}
  /* USER CODE END gps_reader */
}

/* nmea_parser function */
void nmea_parser(void const * argument)
{
  /* USER CODE BEGIN nmea_parser */
	T_GPS_DATA *mptr = 0;

	T_MEAS  *rptr = 0;
	osEvent  evt;

	while(1) {
		mptr = osMailAlloc(gps_mail, osWaitForever); // Allocate memory
		evt = osMailGet(mail, osWaitForever);        // wait for mail
		if (evt.status == osEventMail) {
			rptr = evt.value.p;
			//printf ("PARSE: %s \r\n", rptr->frame);



			if(strstr(rptr->frame,",,,"))
			{
				mptr->fix = 0;
				mptr->lat = 0;
				mptr->lon = 0;
			}
			else if (strstr(rptr->frame,"GGA"))
			{
				mptr->fix = 1;

				strtok(rptr->frame, ",");
				strcpy(mptr->time,strtok(NULL, ","));
				strcpy(mptr->latitude,strtok(NULL, ","));
				strcpy(mptr->latitude_s,strtok(NULL, ","));
				strcpy(mptr->longitude,strtok(NULL, ","));
				strcpy(mptr->longitude_s,strtok(NULL, ","));
				strcpy(mptr->fix_quality,strtok(NULL, ","));
				strcpy(mptr->satellites,strtok(NULL, ","));
				strcpy(mptr->dillution,strtok(NULL, ","));
				strcpy(mptr->altitude,strtok(NULL, ","));

				mptr->lat = _nmea2dec(mptr->latitude, 1, mptr->latitude_s);
				mptr->lon = _nmea2dec(mptr->longitude, 2, mptr->longitude_s);

				//printf("[DBG] GPS Data sent \r\n");
			}
			else
			{
				mptr->fix = 0;
			}
			osMailPut(gps_mail, mptr); // Send Mail
			osMailFree(mail, rptr); // free memory allocated for mail
		}
	}
  /* USER CODE END nmea_parser */
}

/* led_blinker function */
void led_blinker(void const * argument)
{
  /* USER CODE BEGIN led_blinker */
	HAL_GPIO_TogglePin(LED1_GPIO_Port, LED1_Pin);
	osDelay(400);
	HAL_GPIO_TogglePin(LED2_GPIO_Port, LED2_Pin);
	osDelay(400);
	HAL_GPIO_TogglePin(LED3_GPIO_Port, LED3_Pin);
	osDelay(400);
	while(1)
	{
		osSignalWait (0x01, osWaitForever);
		HAL_GPIO_TogglePin(LED1_GPIO_Port, LED1_Pin);
		osDelay(100);
		HAL_GPIO_TogglePin(LED2_GPIO_Port, LED2_Pin);
		osDelay(100);
		HAL_GPIO_TogglePin(LED3_GPIO_Port, LED3_Pin);
	}
  /* USER CODE END led_blinker */
}

/* udp_sender function */
void udp_sender(void const * argument)
{
  /* USER CODE BEGIN udp_sender */
	T_GPS_DATA  *rptr = 0;
	osEvent  evt;

	uint8_t udpdata[198];
	uint16_t msgsize;

	osDelay(15000);
	GSM_Init();
	GSM_UDP_Init();

	while(1) {
		evt = osMailGet(gps_mail, osWaitForever);        // wait for mail
		if (evt.status == osEventMail) {
			rptr = evt.value.p;
			if((rptr->fix) == 0)
			{
				printf("[TO UDP] NO FIX \r\n");
				//printf ("[TO UDP] No Fix\r\n", rptr->lat, rptr->lon, rptr->satellites);
				msgsize = sprintf(udpdata,"NOFIX");
				HAL_UART_Transmit(&huart4, udpdata, msgsize, 100);
			}
			else
			{
				//printf ("[TO UDP FIX] MSG: %3.6f | %3.6f \r\n", rptr->lat, rptr->lon);
				msgsize = sprintf(udpdata,"[GPS],%3.6f,%3.6f,%s", rptr->lat, rptr->lon, rptr->satellites);
				HAL_UART_Transmit(&huart4, udpdata, msgsize, 100);
			}
			osMailFree(gps_mail, rptr); // free memory allocated for mail
		}
	}
  /* USER CODE END udp_sender */
}

/* mpu_reader function */

void mpu_reader(void const * argument)
{
  /* USER CODE BEGIN mpu_reader */

	HAL_StatusTypeDef Status;
	uint32_t srcRows, srcColumns; /* Temporary variables */
	osDelay(3000);
	Status = AccelerometerInit();
	if (Status == HAL_OK) {
		osDelay(1000);
		printf("MPU6050 initialization completed\n\r");
	} else {
		osDelay(1000);
		printf("MPU6050 FAIL\n\r");
	}

	/* Inicjalizacja zmiennych */
	d_t = 0.01;

	/* Inicjalizacja macierzy A */
	const float32_t A_f32[4] =
	{
	  1.0,   -d_t,
	  0,     1.0,
	};
	srcRows = 2;
	srcColumns = 2;
	arm_mat_init(&A, srcRows, srcColumns, A_f32);

	/* Inicjalizacja wektora B */
	const float32_t B_f32[2] = { d_t,   0,};
	srcRows = 2;
	srcColumns = 1;
	arm_mat_init_f32(&B, srcRows, srcColumns, B_f32);

	/* Inicjalizacja wektora C */
	const float32_t C_f32[2] = {    1,   0,	};
	srcRows = 1;
	srcColumns = 2;
	arm_mat_init_f32(&C, srcRows, srcColumns, (float32_t *)C_f32);

	/* Inicjalizacja macierzy V */
	std_dev_v = 1;
	std_dev_w = 2;
	const float32_t V_f32[4] =
	{
		std_dev_v*std_dev_v*d_t,   0,
		0,					       std_dev_v*std_dev_v*d_t,
	};
	srcRows = 2;
	srcColumns = 2;
	arm_mat_init_f32(&V, srcRows, srcColumns, (float32_t *)V_f32);

	/* Inicjalizacja W */
	W = std_dev_w*std_dev_w;

	/* Wartosci poczatkowe filtru */
	/* Inicjalizacja macierzy P */
	P_post_pitch[0] = 1;
	P_post_pitch[1] = 0;
	P_post_pitch[2] = 0;
	P_post_pitch[3] = 1;

	P_post_roll[0] = 1;
	P_post_roll[1] = 0;
	P_post_roll[2] = 0;
	P_post_roll[3] = 1;

	P_post_yaw[0] = 1;
	P_post_yaw[1] = 0;
	P_post_yaw[2] = 0;
	P_post_yaw[3] = 1;

	srcRows = 2;
	srcColumns = 2;
	arm_mat_init_f32(&P_post_p, srcRows, srcColumns, (float32_t *)P_post_pitch);
	arm_mat_init_f32(&P_post_r, srcRows, srcColumns, (float32_t *)P_post_roll);
	arm_mat_init_f32(&P_post_y, srcRows, srcColumns, (float32_t *)P_post_yaw);

	osDelay(150);
	GetAccelerometerData(&mpu);

	x_post_pitch[0] = atan2f((float)mpu.acc_x, (float)mpu.acc_z)*180/M_PI;
	x_post_pitch[1] = 0;
	x_post_roll[0] = atan2f((float)mpu.acc_y, (float)mpu.acc_z)*180/M_PI;
	x_post_roll[1] = 0;
	x_post_yaw[0] = 0;
	x_post_yaw[1] = 0;

	srcRows = 2;
	srcColumns = 1;
	arm_mat_init_f32(&x_post_p, srcRows, srcColumns, (float32_t *)x_post_pitch);
	arm_mat_init_f32(&x_post_r, srcRows, srcColumns, (float32_t *)x_post_roll);
	arm_mat_init_f32(&x_post_y, srcRows, srcColumns, (float32_t *)x_post_yaw);

////////////////////////////////////////////////////////////////

	arm_mat_init_f32(&Ax, 2, 1, (float32_t *)Ax_f32);
	arm_mat_init_f32(&Bu, 2, 1, (float32_t *)Bu_f32);
	arm_mat_init_f32(&x_pri, 2, 1, (float32_t *)x_pri_f32);

	arm_mat_init_f32(&AP, 2, 2, (float32_t *)AP_f32);
	arm_mat_init_f32(&AT, 2, 2, (float32_t *)AT_f32);
	arm_mat_init_f32(&APAT, 2, 2, (float32_t *)APAT_f32);
	arm_mat_init_f32(&P_pri, 2, 2, (float32_t *)P_pri_f32);

	arm_mat_init_f32(&Cx, 1, 1, (float32_t *)Cx_f32);

	arm_mat_init_f32(&CP, 1, 2, (float32_t *)CP_f32);
	arm_mat_init_f32(&CPCT, 1, 1, (float32_t *)CPCT_f32);

	arm_mat_init_f32(&S, 1, 1, (float32_t *)S_f32);
	arm_mat_init_f32(&S1, 1, 1, (float32_t *)S1_f32);

	arm_mat_init_f32(&K, 2, 1, (float32_t *)K_f32);
	arm_mat_init_f32(&eps, 1, 1, (float32_t *)eps_f32);
	arm_mat_init_f32(&Keps, 2, 1, (float32_t *)Keps_f32);

	arm_mat_init_f32(&eps, 1, 1, (float32_t *)eps_f32);
	arm_mat_init_f32(&eps, 1, 1, (float32_t *)eps_f32);

	arm_mat_init_f32(&KS, 2, 1, (float32_t *)KS_f32);
	arm_mat_init_f32(&KSKT, 2, 2, (float32_t *)KSKT_f32);


	/* Infinite loop */
	yaw = 0;
	while(1)
	{
		GetAccelerometerData(&mpu);
		GetGyroData(&mpu);
		//GetMagData(&mpu);

		//PITCH
		u_in[0] = mpu.gyro_y*gsens;
		y_in[0] = atan2f((float)mpu.acc_x, (float)mpu.acc_z)*180/M_PI;
		arm_mat_init_f32(&uk, 1, 1, (float32_t *)u_in);
		arm_mat_init_f32(&yk, 1, 1, (float32_t *)y_in);
		pitch = kalman_filter(&uk,&yk,&x_post_p,&P_post_p);

		//ROLL
		u_in[0] = mpu.gyro_x*gsens;
		y_in[0] = atan2f((float)mpu.acc_y, (float)mpu.acc_z)*180/M_PI;
		arm_mat_init_f32(&uk, 1, 1, (float32_t *)u_in);
		arm_mat_init_f32(&yk, 1, 1, (float32_t *)y_in);
		roll = kalman_filter(&uk,&yk,&x_post_r,&P_post_r);

		//YAW PITCH
		u_in[0] = mpu.gyro_z*gsens;
		y_in[0] = yaw + (mpu.gyro_z*gsens*dt);
		arm_mat_init_f32(&uk, 1, 1, (float32_t *)u_in);
		arm_mat_init_f32(&yk, 1, 1, (float32_t *)y_in);
		yaw = kalman_filter(&uk,&yk,&x_post_y,&P_post_y);

		printf("$%d %d %d;\n\r", (int)pitch, (int)roll, (int)yaw);
		osDelay(10);
	  }
  /* USER CODE END mpu_reader */
}

/* USER CODE BEGIN Application */

float kalman_filter(arm_matrix_instance_f32* u, arm_matrix_instance_f32* y, arm_matrix_instance_f32* x_post, arm_matrix_instance_f32* P_post)
{
	/* x(t+1|t) = Ax(t|t) + Bu(t) */
	arm_mat_mult_f32(&A, x_post, &Ax);
	arm_mat_mult_f32(&B, u, &Bu);
	arm_mat_add_f32(&Ax, &Bu, &x_pri);

	/* P(t+1|t) = AP(t|t)A^T + V */
	arm_mat_mult_f32(&A, &P_post, &AP);
	arm_mat_trans_f32(&A, &AT);
	arm_mat_mult_f32(&AP, &AT, &APAT);
	arm_mat_add_f32(&APAT, &V, &P_pri);

	/* eps(t) = y(t) - Cx(t|t-1) */
	arm_mat_mult_f32(&C, &x_pri, &Cx);

	//arm_mat_sub_f32(&yk,&Cx,&eps);
	eps.pData[0] = y->pData[0] - (Cx.pData[0]); //FIXME

	/* S(t) = CP(t|t-1)C^T + W */
	arm_mat_mult_f32(&C, &P_pri, &CP);
	arm_mat_mult_f32(&CP, &C, &CPCT);

	S.pData[0] = CPCT.pData[0] + W;

	/* K(t) = P(t|t-1)C^TS(t)^-1 */
	arm_mat_mult_f32(&P_pri, &C, &PCT);
	S1.pData[0] = 1/(S.pData[0]);
	arm_mat_mult_f32(&PCT, &S1, &K);

	/* x(t|t) = x(t|t-1) + K(t)eps(t) */
	arm_mat_mult_f32(&K, &eps, &Keps);
	arm_mat_add_f32(&x_pri, &Keps, &x_post);

	/* P(t|t) = P(t|t-1) - K(t)S(t)K(t)^T */
	arm_mat_mult_f32(&K, &S, &KS);
	arm_mat_mult_f32(&KS, &K, &KSKT);
	arm_mat_sub_f32(&P_pri, &KSKT, &P_post);

	return x_post->pData[0];
}

void HAL_UART_RxCpltCallback(UART_HandleTypeDef *huart) {

	if(huart == &huart6)
	{
		osSignalSet (GPSReaderHandle, 0x01); // set the signal 0x0001 for thread tid_thread
	}
}

void GSM_Init()
{
	uint8_t data[20];
	uint16_t MessageSize;

	MessageSize=sprintf(data,"AT\r\n");
	HAL_UART_Transmit(&huart4,data,MessageSize,100);
	osDelay(100);
	HAL_UART_Transmit(&huart4,data,MessageSize,100);
	osDelay(100);
	HAL_UART_Transmit(&huart4,data,MessageSize,100);
	osDelay(100);
}

void GSM_UDP_Init()
{
	uint8_t data[100];
	uint16_t MessageSize;

	osDelay(100);
	printf("udpclose()\r\n");
	MessageSize=sprintf(data,"AT+QICLOSE\r\n");
	while(HAL_UART_Transmit(&huart4,data,MessageSize,100)!=HAL_OK);
	osDelay(300);

	printf("AT+QIFGCNT=0\r\n");
	MessageSize=sprintf(data,"AT+QIFGCNT=0\r\n");
	while(HAL_UART_Transmit(&huart4,data,MessageSize,100)!=HAL_OK);

	osDelay(100);

	printf("AT+QICSGP=1,\"INTERNET\"\r\n");
	MessageSize=sprintf(data,"AT+QICSGP=1,\"INTERNET\"\r\n");
	while(HAL_UART_Transmit(&huart4,data,MessageSize,100)!=HAL_OK);

	osDelay(100);

	printf("AT+QIMUX=0 - NO MUX\r\n");
	MessageSize=sprintf(data,"AT+QIMUX=0\r\n");
	while(HAL_UART_Transmit(&huart4,data,MessageSize,100)!=HAL_OK);

	osDelay(100);

	printf("AT+QIMODE=1 - TRANSPARENT MODE\r\n");
	MessageSize=sprintf(data,"AT+QIMODE=1\r\n");
	while(HAL_UART_Transmit(&huart4,data,MessageSize,100)!=HAL_OK);

	osDelay(100);

	printf("AT+QITCFG=3,2,512,1\r\n");
	MessageSize=sprintf(data,"AT+QITCFG=3,2,512,1\r\n");
	while(HAL_UART_Transmit(&huart4,data,MessageSize,100)!=HAL_OK);

	osDelay(100);

	printf("AT+QIDNSIP=0 - TRANSPARENT MODE\r\n");
	MessageSize=sprintf(data,"AT+QIDNSIP=0\r\n");
	while(HAL_UART_Transmit(&huart4,data,MessageSize,100)!=HAL_OK);

	osDelay(100);

	printf("udpopen()\r\n");
	MessageSize=sprintf(data,"AT+QIOPEN=\"UDP\",\"145.239.92.132\",\"5005\"\r\n");
	while(HAL_UART_Transmit(&huart4,data,MessageSize,100)!=HAL_OK);

	osDelay(100);
}

float _nmea2dec(char *nmea, int type, char *dir)
{
    int idx, dot = 0;
    float dec = 0;
    for (idx=0; idx<strlen(nmea);idx++){
        if (nmea[idx] == '.'){
            dot = idx;
            break;
        }
    }

    if (dot < 3)
        return 0;

    int i,dd;
    float mm;
    char cdd[5], cmm[10];
    memset(&cdd, 0, 5);
    memset(&cmm, 0, 10);
    strncpy(cdd, nmea, dot-2);
    strcpy(cmm, nmea+dot-2);
    dd = atoi(cdd);
    mm = atof(cmm);

    dec = dd + (mm/60);


    if (type == 1 && dec > MAX_LATITUDE)
        return 0;
    else if (type == 2 && dec > MAX_LONGITUDE)
        return 0;

    if (strcmp(dir, "N")== 0 || strcmp(dir, "E")== 0)
      return dec;
    else
      return -1 * dec;
}
/* USER CODE END Application */

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
