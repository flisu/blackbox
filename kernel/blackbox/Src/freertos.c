/**
  ******************************************************************************
  * File Name          : freertos.c
  * Description        : Code for freertos applications
  ******************************************************************************
  * This notice applies to any and all portions of this file
  * that are not between comment pairs USER CODE BEGIN and
  * USER CODE END. Other portions of this file, whether 
  * inserted by the user or by software development tools
  * are owned by their respective copyright owners.
  *
  * Copyright (c) 2017 STMicroelectronics International N.V. 
  * All rights reserved.
  *
  * Redistribution and use in source and binary forms, with or without 
  * modification, are permitted, provided that the following conditions are met:
  *
  * 1. Redistribution of source code must retain the above copyright notice, 
  *    this list of conditions and the following disclaimer.
  * 2. Redistributions in binary form must reproduce the above copyright notice,
  *    this list of conditions and the following disclaimer in the documentation
  *    and/or other materials provided with the distribution.
  * 3. Neither the name of STMicroelectronics nor the names of other 
  *    contributors to this software may be used to endorse or promote products 
  *    derived from this software without specific written permission.
  * 4. This software, including modifications and/or derivative works of this 
  *    software, must execute solely and exclusively on microcontroller or
  *    microprocessor devices manufactured by or for STMicroelectronics.
  * 5. Redistribution and use of this software other than as permitted under 
  *    this license is void and will automatically terminate your rights under 
  *    this license. 
  *
  * THIS SOFTWARE IS PROVIDED BY STMICROELECTRONICS AND CONTRIBUTORS "AS IS" 
  * AND ANY EXPRESS, IMPLIED OR STATUTORY WARRANTIES, INCLUDING, BUT NOT 
  * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY, FITNESS FOR A 
  * PARTICULAR PURPOSE AND NON-INFRINGEMENT OF THIRD PARTY INTELLECTUAL PROPERTY
  * RIGHTS ARE DISCLAIMED TO THE FULLEST EXTENT PERMITTED BY LAW. IN NO EVENT 
  * SHALL STMICROELECTRONICS OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
  * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
  * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, 
  * OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF 
  * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING 
  * NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
  * EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
  *
  ******************************************************************************
  */

/* Includes ------------------------------------------------------------------*/
#include "FreeRTOS.h"
#include "task.h"
#include "cmsis_os.h"

/* USER CODE BEGIN Includes */     
#include <string.h> // strstr, strtok, strcpy
//#include <stdio.h>
#include <stdlib.h> // atof
#include <math.h>
#include "debug.h"	// printf to uart
#include "gpio.h"
#include "usart.h"

/* USER CODE END Includes */

/* Variables -----------------------------------------------------------------*/
/*
 * Thread handlers
 */
osThreadId GPSReaderHandle;
osThreadId NMEAParserHandle;
osThreadId LEDblinkerHandle;
osThreadId UDPSenderHandle;
//osThreadId MPUreaderHandle;

/* USER CODE BEGIN Variables */

#define MAX_LONGITUDE    180
#define MAX_LATITUDE     90

/* UART RX Buffer */
uint8_t received;
size_t gps_frame_size;
char gps_frame[200];

/*
 * Buffer structure for NMEA sentence
 */
typedef struct {
	size_t frame_size;
	char frame[200];
} T_MEAS;

/*
 * Parsed message structure
 */
typedef struct {
	char time[16];
	char latitude[15];
	char latitude_s[2];
	char longitude[15];
	char longitude_s[2];
	char fix_quality[2];
	char satellites[3];
	char dillution[4];
	char altitude[6];

	double lat;
	double lon;

	uint8_t fix;
} T_GPS_DATA;

/*
 * Declaration of message queues
 */
osMailQDef(mail, 16, T_MEAS);
osMailQId  mail;
osMailQDef(gps_mail, 16, T_GPS_DATA);
osMailQId  gps_mail;

/* USER CODE END Variables */

/* Function prototypes -------------------------------------------------------*/
void gps_reader(void const * argument);
void nmea_parser(void const * argument);
void led_blinker(void const * argument);
void udp_sender(void const * argument);
//void mpu_reader(void const * argument);

void MX_FREERTOS_Init(void); /* (MISRA C 2004 rule 8.1) */

/* USER CODE BEGIN FunctionPrototypes */
void GSM_Init();
void GSM_UDP_Init();
float _nmea2dec(char *nmea, int type, char *dir);
//float kalman_filter(float*, float*, float*, float*);
/* USER CODE END FunctionPrototypes */

/* Hook prototypes */

/* Init FreeRTOS */
void MX_FREERTOS_Init(void) {
  /* USER CODE BEGIN Init */
	/*
	 * Create mail queue for parse buffer and UDP sender
	 */
	mail = osMailCreate(osMailQ(mail), NULL);
	gps_mail = osMailCreate(osMailQ(gps_mail), NULL);
  /* USER CODE END Init */


  /* definition and creation of LEDblinker */
  osThreadDef(LEDblinker, led_blinker, osPriorityBelowNormal, 1, 128);
  LEDblinkerHandle = osThreadCreate(osThread(LEDblinker), NULL);

  /* definition and creation of GPSReader */
  osThreadDef(GPSReader, gps_reader, osPriorityNormal, 1, 128);
  GPSReaderHandle = osThreadCreate(osThread(GPSReader), NULL);

  /* definition and creation of NMEAParser */
  osThreadDef(NMEAParser, nmea_parser, osPriorityNormal, 1, 512);
  NMEAParserHandle = osThreadCreate(osThread(NMEAParser), NULL);

  /* definition and creation of UDPSender */
  osThreadDef(UDPSender, udp_sender, osPriorityNormal, 1, 512);
  UDPSenderHandle = osThreadCreate(osThread(UDPSender), NULL);

  /* definition and creation of MPUreader */
  //osThreadDef(MPUreader, mpu_reader, osPriorityBelowNormal, 1, 512);
  //MPUreaderHandle = osThreadCreate(osThread(MPUreader), NULL);
}

/* gps_reader function */
/*
 * Waits for RX signal and then do the reading
 */
void gps_reader(void const * argument)
{
  /* USER CODE BEGIN gps_reader */
	T_MEAS *mptr = 0;

	memset(gps_frame, '\0', sizeof(gps_frame));
	osDelay(15000);

	HAL_UART_Receive_DMA(&huart6, &received, 1);

	while(1)
	{
		osSignalWait (0x01, osWaitForever); // wait forever for the signal 0x01

		if((received == '\r') || (received == '\n'))
		{
			if(received == '\r')
			{
				mptr = osMailAlloc(mail, osWaitForever); // Allocate memory
				mptr->frame_size = sprintf(mptr->frame, gps_frame);
				osMailPut(mail, mptr); // Send Mail
				osSignalSet (LEDblinkerHandle, 0x01);
				gps_frame_size = 0;
			}
		}
		else
		{
			gps_frame[gps_frame_size] = received;
			gps_frame_size++;
		}
	}
  /* USER CODE END gps_reader */
}

/* nmea_parser function */
/*
 * Parser for NMEA GGA sentence.
 * Waits for message and checks for GNSS fix.
 */
void nmea_parser(void const * argument)
{
  /* USER CODE BEGIN nmea_parser */
	T_GPS_DATA *mptr = 0;

	T_MEAS  *rptr = 0;
	osEvent  evt;

	/*
	 * waits for buffered message,
	 * checks for GNSS fix,
	 * passes parsed message to UDP_Sender
	 */
	while(1) {
		mptr = osMailAlloc(gps_mail, osWaitForever); // Allocate memory
		evt = osMailGet(mail, osWaitForever);        // wait for mail
		if (evt.status == osEventMail) {
			rptr = evt.value.p;
			//printf ("[DEBUG] PARSE: %s \r\n", rptr->frame);

			if(strstr(rptr->frame,",,,")) // NOFIX condition
			{
				mptr->fix = 0;
				mptr->lat = 0;
				mptr->lon = 0;
			}
			else if (strstr(rptr->frame,"GGA")) // FIX condidion
			{
				mptr->fix = 1;

				strtok(rptr->frame, ",");
				strcpy(mptr->time,strtok(NULL, ","));
				strcpy(mptr->latitude,strtok(NULL, ","));
				strcpy(mptr->latitude_s,strtok(NULL, ","));
				strcpy(mptr->longitude,strtok(NULL, ","));
				strcpy(mptr->longitude_s,strtok(NULL, ","));
				strcpy(mptr->fix_quality,strtok(NULL, ","));
				strcpy(mptr->satellites,strtok(NULL, ","));
				strcpy(mptr->dillution,strtok(NULL, ","));
				strcpy(mptr->altitude,strtok(NULL, ","));

				mptr->lat = _nmea2dec(mptr->latitude, 1, mptr->latitude_s);
				mptr->lon = _nmea2dec(mptr->longitude, 2, mptr->longitude_s);

				//printf("[DBG] GPS Data sent \r\n");
			}
			else
			{
				mptr->fix = 0;
			}
			osMailPut(gps_mail, mptr); // Send Mail
			osMailFree(mail, rptr); // free memory allocated for mail
		}
	}
  /* USER CODE END nmea_parser */
}

/* led_blinker function */
/*
 * Indicate reading from GNSS with snake on leds.
 */
void led_blinker(void const * argument)
{
  /* USER CODE BEGIN led_blinker */
	HAL_GPIO_TogglePin(LED1_GPIO_Port, LED1_Pin);
	osDelay(400);
	HAL_GPIO_TogglePin(LED2_GPIO_Port, LED2_Pin);
	osDelay(400);
	HAL_GPIO_TogglePin(LED3_GPIO_Port, LED3_Pin);
	osDelay(400);
	while(1)
	{
		osSignalWait (0x01, osWaitForever);
		HAL_GPIO_TogglePin(LED1_GPIO_Port, LED1_Pin);
		osDelay(100);
		HAL_GPIO_TogglePin(LED2_GPIO_Port, LED2_Pin);
		osDelay(100);
		HAL_GPIO_TogglePin(LED3_GPIO_Port, LED3_Pin);
	}
  /* USER CODE END led_blinker */
}

/* udp_sender function */
void udp_sender(void const * argument)
{
  /* USER CODE BEGIN udp_sender */
	T_GPS_DATA  *rptr = 0;	//
	osEvent  evt;

	uint8_t udpdata[198]; 	// printing buffer
	uint16_t msgsize;		//

	/* Wait for network connection with GSM
	 * TODO: check with AT command */
	osDelay(15000);
	GSM_Init();				// run AT Check
	GSM_UDP_Init();			// run UDP initalization

	/*
	 * Wait for parsed message and transmit position or NOFIX to listener
	 */
	while(1) {
		evt = osMailGet(gps_mail, osWaitForever);	// wait for mail
		if (evt.status == osEventMail) {
			rptr = evt.value.p;
			if((rptr->fix) == 0)
			{
				printf("[TO UDP] NO FIX \r\n");
				//printf ("[TO UDP] No Fix\r\n", rptr->lat, rptr->lon, rptr->satellites);
				msgsize = sprintf(udpdata,"NOFIX");
				HAL_UART_Transmit(&huart4, udpdata, msgsize, 100);
			}
			else
			{
				printf ("[TO UDP FIX] MSG: %3.6f | %3.6f \r\n", rptr->lat, rptr->lon);
				msgsize = sprintf(udpdata,"[GPS],%3.6f,%3.6f,%s", rptr->lat, rptr->lon, rptr->satellites);
				HAL_UART_Transmit(&huart4, udpdata, msgsize, 100);
			}
			osMailFree(gps_mail, rptr); // free memory allocated for mail
		}
	}
  /* USER CODE END udp_sender */
}

/* USER CODE BEGIN Application */
void HAL_UART_RxCpltCallback(UART_HandleTypeDef *huart) {

	/*
	 * Reads callback from UART RX and buffers message in other thread
	 */
	if(huart == &huart6)
	{
		// Set signal  0x01 for GPSReader thread
		osSignalSet (GPSReaderHandle, 0x01);
	}
}

/*
 * Autobaudrate check with three "AT"'s
 */
void GSM_Init()
{
	uint8_t data[20];
	uint16_t MessageSize;

	MessageSize=sprintf(data,"AT\r\n");
	HAL_UART_Transmit(&huart4,data,MessageSize,100);
	osDelay(100);
	HAL_UART_Transmit(&huart4,data,MessageSize,100);
	osDelay(100);
	HAL_UART_Transmit(&huart4,data,MessageSize,100);
	osDelay(100);
}

/*
 * UDP initialization commands
 * map.flisu.pl - 145.239.92.132:5005
 */
void GSM_UDP_Init()
{
	uint8_t data[100];
	uint16_t MessageSize;

	osDelay(100);
	printf("udpclose()\r\n");
	MessageSize=sprintf(data,"AT+QICLOSE\r\n");
	while(HAL_UART_Transmit(&huart4,data,MessageSize,100)!=HAL_OK);
	osDelay(300);

	printf("AT+QIFGCNT=0\r\n");
	MessageSize=sprintf(data,"AT+QIFGCNT=0\r\n");
	while(HAL_UART_Transmit(&huart4,data,MessageSize,100)!=HAL_OK);

	osDelay(100);

	printf("AT+QICSGP=1,\"INTERNET\"\r\n");
	MessageSize=sprintf(data,"AT+QICSGP=1,\"INTERNET\"\r\n");
	while(HAL_UART_Transmit(&huart4,data,MessageSize,100)!=HAL_OK);

	osDelay(100);

	printf("AT+QIMUX=0 - NO MUX\r\n");
	MessageSize=sprintf(data,"AT+QIMUX=0\r\n");
	while(HAL_UART_Transmit(&huart4,data,MessageSize,100)!=HAL_OK);

	osDelay(100);

	printf("AT+QIMODE=1 - TRANSPARENT MODE\r\n");
	MessageSize=sprintf(data,"AT+QIMODE=1\r\n");
	while(HAL_UART_Transmit(&huart4,data,MessageSize,100)!=HAL_OK);

	osDelay(100);

	printf("AT+QITCFG=3,2,512,1\r\n");
	MessageSize=sprintf(data,"AT+QITCFG=3,2,512,1\r\n");
	while(HAL_UART_Transmit(&huart4,data,MessageSize,100)!=HAL_OK);

	osDelay(100);

	printf("AT+QIDNSIP=0 - TRANSPARENT MODE\r\n");
	MessageSize=sprintf(data,"AT+QIDNSIP=0\r\n");
	while(HAL_UART_Transmit(&huart4,data,MessageSize,100)!=HAL_OK);

	osDelay(100);

	printf("udpopen()\r\n");
	MessageSize=sprintf(data,"AT+QIOPEN=\"UDP\",\"145.239.92.132\",\"5005\"\r\n");
	while(HAL_UART_Transmit(&huart4,data,MessageSize,100)!=HAL_OK);

	osDelay(100);
}

/*
 * NMEA sentence to decimal conversion
 * TODO: do it on server side
 */
float _nmea2dec(char *nmea, int type, char *dir)
{
    int idx, dot = 0;
    float dec = 0;
    for (idx=0; idx<strlen(nmea);idx++){
        if (nmea[idx] == '.'){
            dot = idx;
            break;
        }
    }

    if (dot < 3)
        return 0;

    int i,dd;
    float mm;
    char cdd[5], cmm[10];
    memset(&cdd, 0, 5);
    memset(&cmm, 0, 10);
    strncpy(cdd, nmea, dot-2);
    strcpy(cmm, nmea+dot-2);
    dd = atoi(cdd);
    mm = atof(cmm);

    dec = dd + (mm/60);

    if (type == 1 && dec > MAX_LATITUDE)
        return 0;
    else if (type == 2 && dec > MAX_LONGITUDE)
        return 0;

    if (strcmp(dir, "N")== 0 || strcmp(dir, "E")== 0)
      return dec;
    else
      return -1 * dec;
}
/* USER CODE END Application */

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
