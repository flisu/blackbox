/*
 * debug.c
 *
 *  Created on: 28.11.2017
 *      Author: flisu
 */

#include "debug.h"

int _write(int file, char *ptr, int len)
{
	HAL_UART_Transmit(&huart1, ptr, len, 1000);
	return len;
}
