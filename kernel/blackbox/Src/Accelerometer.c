#include "i2c.h"
#include "gpio.h"
#include "debug.h"
#include "Accelerometer.h"

HAL_StatusTypeDef GetAccelerometerData(MPU_6050* mpu)
{
	uint8_t data[6];
	uint8_t reg = MPU6050_ACC_XOUT_H;
	uint8_t address = MPU6050_I2C_ADDR | MPU6050_Device_0;

	/* Read accelerometer data */
	while(HAL_I2C_Master_Transmit(&ACC_I2C_HANDLE, (uint16_t)address, &reg, 1, 1000) != HAL_OK);

	while(HAL_I2C_Master_Receive(&ACC_I2C_HANDLE, (uint16_t)address, data, 6, 1000) != HAL_OK);

	/* Format */
	mpu->acc_x = (int16_t)(data[0] << 8 | data[1]);
	mpu->acc_y = (int16_t)(data[2] << 8 | data[3]);
	mpu->acc_z = (int16_t)(data[4] << 8 | data[5]);

	/* Return OK */
	return HAL_OK;
}

HAL_StatusTypeDef GetGyroData(MPU_6050* mpu)
{
	uint8_t data[6];
	uint8_t reg = MPU6050_GYRO_XOUT_H;;
	uint8_t address = MPU6050_I2C_ADDR | MPU6050_Device_0;

	/* Read accelerometer data */
	while(HAL_I2C_Master_Transmit(&ACC_I2C_HANDLE, (uint16_t)address, &reg, 1, 1000) != HAL_OK);

	while(HAL_I2C_Master_Receive(&ACC_I2C_HANDLE, (uint16_t)address, data, 6, 1000) != HAL_OK);

	/* Format */
	mpu->gyro_x = (int16_t)(data[0] << 8 | data[1]);
	mpu->gyro_y = (int16_t)(data[2] << 8 | data[3]);
	mpu->gyro_z = (int16_t)(data[4] << 8 | data[5]);

	/* Return OK */
	return HAL_OK;
}

HAL_StatusTypeDef GetMagData(MPU_6050* mpu)
{
	uint8_t data[7];
	uint8_t reg = AK8963_XOUT_L;
	uint8_t address = AK8963_ADDRESS;

	reg = AK8963_ST1;
	address = AK8963_ADDRESS;

	HAL_I2C_Master_Transmit(&ACC_I2C_HANDLE, (uint16_t)address, &reg, 1, 1000);
	HAL_I2C_Master_Receive(&ACC_I2C_HANDLE, (uint16_t)address, data, 1, 1000);

	if(data[0] & 0x01)
	{
		reg = AK8963_XOUT_L;
		address = AK8963_ADDRESS;

		HAL_I2C_Master_Transmit(&ACC_I2C_HANDLE, (uint16_t)address, &reg, 1, 1000);
		HAL_I2C_Master_Receive(&ACC_I2C_HANDLE, (uint16_t)address, data, 7, 1000);

		/* Format */
		uint8_t c = data[6]; // End data read by reading ST2 register
		if(!(c & 0x08)) { // Check if magnetic sensor overflow set, if not then report data

			mpu->mag_x = (int16_t)(data[1] << 8 | data[0]);
			mpu->mag_y = (int16_t)(data[3] << 8 | data[2]);
			mpu->mag_z = (int16_t)(data[5] << 8 | data[4]);
			printf("[mag] %d %d %d\r\n", mpu->mag_x, mpu->mag_y, mpu->mag_z );
		}
	}
	/* Return OK */
	return HAL_OK;
}

HAL_StatusTypeDef MagInit(){
	uint8_t reg = 0x00;
	uint8_t received = 0x00;
	uint8_t d[2];

	uint8_t WHO_AM_I = AK8963_WHO_AM_I;
	uint8_t address = I2C_SLV0_DO | READ_FLAG;

	uint8_t wData = 0x01;
	/* Check who am I */
	//------------------
	/* Send address */
	if (HAL_I2C_Master_Transmit(&ACC_I2C_HANDLE, address, &WHO_AM_I, 1, 1000)
			!= HAL_OK) {
		printf("[DEBUG] Whoami address send error. \n\r");
	}/*
	if(HAL_I2C_Mem_Write(&ACC_I2C_HANDLE, AK8963_ADDRESS, AK8963_CNTL2,1,&wData, 1, 100)
				!= HAL_OK) {
			printf("[DEBUG] SOFT RESET send error. \n\r");
			return HAL_ERROR;
		}*/
	/* Check who am I */
	//------------------
	/* Send address */
/*
	if(HAL_I2C_Mem_Read(&ACC_I2C_HANDLE, AK8963_ADDRESS, AK8963_WHO_AM_I,1,&received, 1, 100)
			!= HAL_OK) {
		printf("[DEBUG] Whoami address send error. \n\r");
		return HAL_ERROR;
	}*/

	/* Receive multiple byte */
	if (HAL_I2C_Master_Receive(&ACC_I2C_HANDLE, address, &received, 1, 100)
			!= HAL_OK) {
		printf("[DEBUG] Receive whoami error. \n\r");
	}
	/* Checking */
	if (received != 0x48) {
		/* Return error */
		printf("[DEBUG / CRITICAL MASSIVE ASSERTION ERROR] I DONT KNOW WHO AM I. \n\r");
		return HAL_ERROR;
	}
	/*
	uint8_t data[3];  // x/y/z gyro calibration data stored here
	uint8_t val;
	uint8_t reg;
	uint8_t address;

	val = 0x00;
	reg = AK8963_CNTL;
	address = AK8963_ADDRESS;
	HAL_I2C_Mem_Write(&ACC_I2C_HANDLE, (uint16_t)address, reg, 1, &val, 1, 100);

	val = 0x0F;
	reg = AK8963_CNTL;
	address = AK8963_ADDRESS;
	HAL_I2C_Mem_Write(&ACC_I2C_HANDLE, (uint16_t)address, reg, 1, &val, 1, 100);


	reg = AK8963_ASAX;
	address = AK8963_ADDRESS;
	HAL_I2C_Master_Transmit(&ACC_I2C_HANDLE, (uint16_t)address, &reg, 1, 1000);
	HAL_I2C_Master_Receive(&ACC_I2C_HANDLE, (uint16_t)address, data, 3, 1000);


	mpu->magsens[0] =  (float)(data[0] - 128)/256.0f + 1.0f;   // Return x-axis sensitivity adjustment values, etc.
	mpu->magsens[1] =  (float)(data[1] - 128)/256.0f + 1.0f;
	mpu->magsens[2] =  (float)(data[2] - 128)/256.0f + 1.0f;


	val = 0x00;
	reg = AK8963_CNTL;
	address = AK8963_ADDRESS;
	HAL_I2C_Mem_Write(&ACC_I2C_HANDLE, (uint16_t)address, reg, 1, &val, 1, 100);
	osDelay(10);
	// Configure the magnetometer for continuous read and highest resolution
	// set Mscale bit 4 to 1 (0) to enable 16 (14) bit resolution in CNTL register,
	// and enable continuous mode data acquisition Mmode (bits [3:0]), 0010 for 8 Hz and 0110 for 100 Hz sample rates

	val = MFS_16BITS << 4 | 0x06;
	reg = AK8963_CNTL;
	address = AK8963_ADDRESS;
	HAL_I2C_Mem_Write(&ACC_I2C_HANDLE, (uint16_t)address, reg, 1, &val, 1, 100);
*/
	return HAL_OK;
}

HAL_StatusTypeDef AccelerometerInit() {
	uint8_t reg = 0x00;
	uint8_t received = 0x00;
	uint8_t d[2];

	uint8_t WHO_AM_I = MPU6050_WHO_AM_I;
	uint8_t address = MPU6050_I2C_ADDR | MPU6050_Device_0;

	/* Check who am I */
	//------------------
	/* Send address */
	if (HAL_I2C_Master_Transmit(&ACC_I2C_HANDLE, address, &WHO_AM_I, 1, 1000)
			!= HAL_OK) {
		printf("[DEBUG] Whoami address send error. \n\r");
	}
	/* Receive multiple byte */
	if (HAL_I2C_Master_Receive(&ACC_I2C_HANDLE, address, &received, 1, 1000)
			!= HAL_OK) {
		printf("[DEBUG] Receive whoami error. \n\r");
	}
	/* Checking */
	if (received != MPU6050_I_AM) {
		/* Return error */
		printf("[DEBUG / CRITICAL MASSIVE ASSERTION ERROR] I DONT KNOW WHO AM I. \n\r");
	}
	//------------------
	/* Wakeup MPU6050 */
	//------------------
	/* Format array to send */
	//uint8_t address = MPU6050_PWR_MGMT1 | MPU6050_Device_0;
	d[0] = MPU6050_PWR_MGMT1;
	d[1] = 0x00;
	/* Try to transmit via I2C */
	if (HAL_I2C_Master_Transmit(&ACC_I2C_HANDLE, (uint16_t) address, (uint8_t *) d, 2, 1000) != HAL_OK) {
		printf("[DEBUG] Wakeup error. \n\r");
	}
	//------------------
	/* Set sample rate to 1kHz */
	/* Format array to send */
	d[0] = MPU6050_SMPLRT_DIV;
	d[1] = MPU6050_DataRate_1KHz;
	while (HAL_I2C_Master_Transmit(&ACC_I2C_HANDLE, (uint16_t) address, (uint8_t *) d, 2, 1000) != HAL_OK);

	/* Config accelerometer */
	reg =(uint8_t )MPU6050_ACC_CONFIG;
	while(HAL_I2C_Master_Transmit(&ACC_I2C_HANDLE, (uint16_t)address, &reg, 1, 1000) != HAL_OK);
	while(HAL_I2C_Master_Receive(&ACC_I2C_HANDLE, (uint16_t)address, &received, 1, 1000) != HAL_OK);
	received = (received & 0xE7) | (uint8_t)MPU6050_Accelerometer_2G << 3;
	while(HAL_I2C_Master_Transmit(&ACC_I2C_HANDLE, (uint16_t)address, &received, 1, 1000) != HAL_OK);

	gsens = (float)1 / MPU6050_GYRO_SENS_250;
	asens = (float)1 / MPU6050_ACCE_SENS_2;

	/* Config Gyroscope */
	reg =(uint8_t )MPU6050_GYRO_CONFIG;
	while(HAL_I2C_Master_Transmit(&ACC_I2C_HANDLE, (uint16_t)address,&reg, 1, 1000) != HAL_OK);
	while(HAL_I2C_Master_Receive(&ACC_I2C_HANDLE, (uint16_t)address, &received, 1, 1000) != HAL_OK);
	received = (received & 0xE7) | (uint8_t)MPU6050_Gyroscope_250s << 3;
	while(HAL_I2C_Master_Transmit(&ACC_I2C_HANDLE, (uint16_t)address,&received, 1, 1000) != HAL_OK);

	/* Return OK */
	return HAL_OK;
}

void ComplementaryFilter(MPU_6050* mpu, float *pitch, float *roll)
{
    float pitchAcc, rollAcc;
    // Integrate the gyroscope data -> int(angularSpeed) = angle
    *pitch += (float)mpu->gyro_x * gsens * dt; // Angle around the X-axis
    *roll -= (float)mpu->gyro_y * gsens * dt;    // Angle around the Y-axis

    // Compensate for drift with accelerometer data if !bullshit
    // Sensitivity = -2 to 2 G at 16Bit -> 2G = 32768 && 0.5G = 8192
    int forceMagnitudeApprox = abs(mpu->acc_x) + abs(mpu->acc_y) + abs(mpu->acc_z);
    if (forceMagnitudeApprox > 8192 && forceMagnitudeApprox < 32768)
    {
	// Turning around the X axis results in a vector on the Y-axis
        pitchAcc = atan2f((float)mpu->acc_y, (float)mpu->acc_z) * 180 / M_PI;
        *pitch = *pitch * 0.98 + pitchAcc * 0.02;

	// Turning around the Y axis results in a vector on the X-axis
        rollAcc = atan2f((float)mpu->acc_x, (float)mpu->acc_z) * 180 / M_PI;
        *roll = *roll * 0.98 + rollAcc * 0.02;
    }
}
